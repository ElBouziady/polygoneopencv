#include "mainwindow.h"
#include <QApplication>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <QTextStream>
#include <QString>
#include <QFile>

using namespace cv;
using namespace std;

bool IsRectInside(Rect test,string path){

    //Mat img = imread(path.c_str());
    //imshow( "Polygon Test", img );

    vector<Point> contour;
    contour.push_back(Point(739,27));
    contour.push_back(Point(561,125));
    contour.push_back(Point(391,208));
    contour.push_back(Point(144,370));
    contour.push_back(Point(5,718));
    contour.push_back(Point(1,1020));
    contour.push_back(Point(301,1022));
    contour.push_back(Point(710,1022));
    contour.push_back(Point(714,373));
    contour.push_back(Point(783,273));
    contour.push_back(Point(892,170));
    contour.push_back(Point(901,66));
    contour.push_back(Point(943,2));
    contour.push_back(Point(778,5));

    // create a pointer to the data as an array of points (via a conversion to
    // a Mat() object)

    const cv::Point *pts = (const cv::Point*) Mat(contour).data;
    int npts = Mat(contour).rows;

    std::cout << "Number of polygon vertices: " << npts << std::endl;

    // draw the polygon
/*
    polylines(img, &pts,&npts, 1,
              true, 			// draw closed contour (i.e. joint end to start)
              Scalar(0,255,0),// colour RGB ordering (here = green)
              3, 		        // line thickness
              CV_AA, 0);





    rectangle(img, test, Scalar(0, 255, 255), 3, 8, 0); // RED point
*/

    // define a polygon (as a vector of points)




    Point2f test_pt1(test.x,test.y);
    Point2f test_pt2(test.x,test.y+test.height);
    Point2f test_pt3(test.x+test.width,test.y);
    Point2f test_pt4(test.x+test.width,test.y+test.height);
    if ((pointPolygonTest(Mat(contour), test_pt1, true) > 0) && (pointPolygonTest(Mat(contour), test_pt2, true) > 0)
            && (pointPolygonTest(Mat(contour), test_pt3, true) > 0) && (pointPolygonTest(Mat(contour), test_pt4, true) > 0)){
        cout<<"In"<<endl;
       /* namedWindow("Polygon Test", 0);
        imshow( "Polygon Test", img );
        waitKey(0);*/
        return true;
    }
    else {
        cout<<"Out"<<endl;
        return false;
    }


}

double Overlap(Rect a, Rect b){
    Rect intersection = a & b;
    return (double)intersection.area()/(a.area() + b.area() - intersection.area());
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    cout<<Overlap(Rect(0,0,1,1),Rect(1,1,2,2))<<endl;
/*
    Rect A(20,20,80,80); // blue
    Rect B(60,60,60,60); // blue

    Rect C = A & B;   // red
    Rect D = A | B;   // green

    cerr << "A" << A << endl;
    cerr << "B" << B << endl;
    cerr << "C" << C << endl;
    cerr << "D" << D << endl;

    Mat draw(200,200,CV_8UC3,Scalar::all(0));
    rectangle(draw,A,Scalar(200,0,0),2);
    rectangle(draw,B,Scalar(200,0,0),2);
    rectangle(draw,C,Scalar(0,0,200),1);
    rectangle(draw,D,Scalar(0,200,0),1);

    imshow("Draw",draw);
    waitKey(0);
*/

    QFile dfin("/home/boukary/Documents/Untitled Folder/predictedLocationsDetection.txt");
    QFile dfout("/home/boukary/Documents/Untitled Folder/predictedLocationsDetection_out.txt");

    if (!dfin.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return 1;
    }
    if (!dfout.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        return 1;
    }

    QTextStream txStreamin(&dfin);
    QTextStream txStreamout(&dfout);

    while (!txStreamin.atEnd()) {
        cout<<"read line"<<endl;
        QString text = txStreamin.readLine();
        QStringList splitD = text.split(" ");
        cout<<splitD[0].toStdString()<<endl;
        txStreamout<<splitD[0];

        QStringList new_rect;
        for(int j=2;j<splitD.size();j+=4)
        {
            if (IsRectInside(Rect(splitD[j].toInt(),splitD[j+1].toInt(),splitD[j+2].toInt(),splitD[j+3].toInt()),splitD[0].toStdString())){
                new_rect.push_back(splitD[j]);
                new_rect.push_back(splitD[j+1]);
                new_rect.push_back(splitD[j+2]);
                new_rect.push_back(splitD[j+3]);
            }
        }

        txStreamout<<" "<<new_rect.size()/4;

        for(int j=0;j<new_rect.size();j++)
        {
            txStreamout<<" "<<new_rect[j];

        }
        txStreamout<<endl;
    }

    return a.exec();
}
